ARCH=amd64
VERSION=11.1.0.8372

all:
	echo "Run make install"

install:
	env DESTDIR=$(DESTDIR) VERSION=$(VERSION) ARCH=$(ARCH) bash wps-repack.sh
